Additional information regarding the GMSL2 based scripts for the Windshield
and Leopard Imaging cameras.

The GMSL2 module requires an additional firmware. This firmware must be
loaded every time during bootup of the system. Therefore the script
"PP15602_ProgrammCrosslinks.sh" was created. During creation of the script
the device names of the cards were unknown. This has to be adapted in the
script.

Do do so:
* Run the follwing command to get an overview of the build in grabber cards:
  sxpfinfo

* The command will print an overview of the mounted grabber cards like this:

  admin@SerEvalPC1:~$ sxpfinfo
  sxpf library revision: r3390
  sxpf kernel module revision: r3390
  Found 1 SX ProFrame card in the system:
  #0 -> CAPTURE card, Firmware version 3.7.6
        build date 2019-07-30
        Plasma version 2019-07-25/1
        max. video channels = 4
        8 buffers, max. frame size = 33554432
        I2C capture not supported
        Available bandwidth at slot 1600MByte/s
        Chip temperature 68.9°C / 156°F
          port #0 -> MAX9295/MAX9296A (or MAX96716/MAX96717) adapter (type 17), version 4
                  MachXO creation date 2019-03-06/1
                  CrossLink firmware for MAX9295/MAX9296 Record only (x12) (type 0x13) version 4
                  CrossLink creation date 2019-05-10/1
                  EEPROM INI data capacity: 1016 bytes
                  EEPROM content:
                  #! sxproframe-init-sequence, version=1.0
                  #
                  # This file contains a generated init sequence, recovered from a raw binary image.
                  #
  
                  [aliases]
                  # no informations in binary
  
                  [event-list]
          port #1 -> no adapter
          port #2 -> no adapter
          port #3 -> no adapter
  admin@SerEvalPC1:~$
  
* In this example there is one grabber card at number 0 with one module
  "MAX9295/MAX9296A" at port 0.

* Please update the card numbers in the file "PP15602_ProgrammCrosslinks.sh"
  according to the information from the sxpfinfo command.

Please note:
With the first delivery of the AUTERA systems only one GMSL2 grabber card is
installed. Thus, the card numbers might change after installing the second
one.