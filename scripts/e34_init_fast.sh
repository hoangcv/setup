#!/bin/bash

CONFIG_DIR="/opt/e34/configs"

## camera initilization

camera_init(){
    echo "Resetting and Programming crossing for /dev/sxpf$1 at port $2" 
    while [[ "$(sxpf-init-sequence /dev/sxpf$1 -port $2 -ini $CONFIG_DIR/Crosslink/PP15602_SwitchOffCameraPort.ini  --execute 0)" =~ "failed" ]]
    do
        echo "Failed to reset the camera port for /dev/sxpf$1 at port $2. Sleeping 3 sec."
        sleep 3
    done
    echo "Successfully resetted camera port for /dev/sxpf$1 at port $2."
    while [[ "$(sxpfclinit $1 -b $2 -B 400000 $CONFIG_DIR/Crosslink/05_max9295_9296_crosslink_REC_X12_algo.iea $CONFIG_DIR/Crosslink/05_max9295_9296_crosslink_REC_X12_data.ied)" =~ "FAIL" ]]
    do
        echo "Failed to initalize the camera port for /dev/sxpf$1 at port $2. Sleeping 3 sec."
        sleep 3
    done
    echo "Successfully initialized camera port for /dev/sxpf$1 at port $2."
}

camera_init 0 0 
camera_init 0 3
camera_init 1 0


## network initilization
echo "up can interface"
ifconfig can0 up 
ifconfig can1 up
ifconfig can2 up
ifconfig can3 up
ifconfig can4 up
ifconfig can5 up
ifconfig can6 up
ifconfig can7 up
